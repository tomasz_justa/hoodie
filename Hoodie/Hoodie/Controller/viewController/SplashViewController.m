//
//  SplashViewController.m
//  Hoodie
//
//  Created by Tomasz Justa on 20.05.2015.
//  Copyright (c) 2015 Monotix. All rights reserved.
//

#import "SplashViewController.h"

#import "SignInViewController.h"
#import "TabBarViewController.h"

@interface SplashViewController ()
{
    
}
@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self performSelector:@selector(showNext) withObject:nil afterDelay:2.0];
    [self startLoading];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
-(void) showNext
{
    [self stopLoading];
    [self performSegueWithIdentifier:@"SignInSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"SignInSegue"])
    {
        SignInViewController *vc = segue.destinationViewController;
        vc.transitioningDelegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"StartSegue"])
    {
        TabBarViewController *vc = segue.destinationViewController;
        vc.transitioningDelegate = self;
    }
}

@end
