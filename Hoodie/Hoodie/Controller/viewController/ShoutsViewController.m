//
//  ShoutsViewController.m
//  Hoodie
//
//  Created by Tomasz Justa on 01.06.2015.
//  Copyright (c) 2015 Monotix. All rights reserved.
//

#import "ShoutsViewController.h"
#import "ShoutExpandTableViewCell.h"
#import "ShoutTableViewCell.h"
#import "RDVTabBarController.h"
#import "RDVTabBarItem.h"

@interface ShoutsViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSIndexPath *selectedPath;
@property (nonatomic, strong) NSMutableArray *expandableCell;
@end

@implementation ShoutsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.selectedPath = [NSIndexPath indexPathForRow:-1 inSection:0];
    [self setupView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup View
-(void) setupView
{
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [[self rdv_tabBarItem] setBadgeValue:@"34"];
    self.expandableCell = [NSMutableArray array];
}

#pragma mark - TableView Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier;
    if ([self.selectedPath isEqual:indexPath] && [self.expandableCell containsObject:indexPath]) {
        identifier = @"ShoutExpandCell";
        ShoutExpandTableViewCell *cell = (ShoutExpandTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
        if(!cell){
            UINib *nib = [UINib nibWithNibName:identifier bundle:nil];
            [tableView registerNib:nib forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        return cell;
    } else {
        identifier = @"ShoutCell";
        ShoutTableViewCell *cell = (ShoutTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
        if(!cell){
            UINib *nib = [UINib nibWithNibName:identifier bundle:nil];
            [tableView registerNib:nib forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.selectedPath isEqual:indexPath] && [self.expandableCell containsObject:indexPath]) {
        return self.tableView.rowHeight*2;
    }else{
        return self.tableView.rowHeight;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSIndexPath *oldPath = self.selectedPath;
    self.selectedPath = indexPath;
    if (![self.expandableCell containsObject:self.selectedPath]){
        [self.expandableCell addObject:indexPath];
    } else {
        [self.expandableCell removeObject:self.selectedPath];
    }
    [self.tableView reloadRowsAtIndexPaths:@[indexPath,oldPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
