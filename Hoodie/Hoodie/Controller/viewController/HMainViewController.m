//
//  HMainViewController.m
//  Hoodie
//
//  Created by Tomasz Justa on 20.05.2015.
//  Copyright (c) 2015 Monotix. All rights reserved.
//

#import "HMainViewController.h"
#import "SVProgressHUD.h"
#import "CRToast.h"

@interface HMainViewController ()
{
    UIView *_loaderView;
    NSMutableDictionary *_messageOptions;
}
@end

@implementation HMainViewController
-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupView];
    [self setupLoader];
    [self setupMessages];
    
    if (!self.service) {
        self.service = [[GTLServiceHoodieApi alloc] init];
        self.service.retryEnabled = YES;
    }
    
    // Listen to the standard keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDoubleTap)];
    doubleTap.numberOfTapsRequired = 2;
    doubleTap.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:doubleTap];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - View Setup
-(void) setupView
{
    UIImage *tileImage = [UIImage imageNamed:@"front_bcg_tiled"];
    self.view.backgroundColor = [UIColor colorWithPatternImage:tileImage];
}

#pragma mark - Messages&Alerts
-(void) setupMessages
{
    _messageOptions = [@{
                              kCRToastTextKey : @"",
                              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypeCover),
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastBackgroundColorKey : [UIColor redColor],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeSpring),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastTimeIntervalKey : @(2.0)
                              } mutableCopy];
}

-(void)showMessage:(NSString*)message
{
    _messageOptions[kCRToastTextKey] = message;
    [CRToastManager showNotificationWithOptions:_messageOptions
                                completionBlock:^{
                                    [self messageDidHideComplete];
                                }];
}

-(void)messageDidHideComplete
{
    
}

#pragma mark - Loader
-(void) setupLoader
{
    [SVProgressHUD setBackgroundColor:[UIColor colorWithRed:152.0/255.0 green:230.0/255.0 blue:0.0/255.0 alpha:1.0]];
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1.0]];
    [SVProgressHUD setRingThickness:2.0];
    
    _loaderView = [[UIView alloc] initWithFrame:self.view.frame];
    _loaderView.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.8];
    _loaderView.hidden = YES;
    [self.view addSubview:_loaderView];
}
-(void) startLoading
{
    [SVProgressHUD show];
    //loaderView.hidden = NO;
}
-(void) startLoadingWithMessage:(NSString *)message
{
    [SVProgressHUD showWithStatus:message];
    //loaderView.hidden = NO;
}
-(void)stopLoading
{
    [SVProgressHUD dismiss];
    //loaderView.hidden = YES;
}

#pragma mark - Keyboard Notifications
-(void) onDoubleTap
{
    [[self view] endEditing:YES];
}

- (void)keyboardWillShow:(NSNotification*)aNotification {
    [self adjustForKeyboard:aNotification showing:YES];
}

- (void)keyboardWillHide:(NSNotification*)aNotification {
    [self adjustForKeyboard:aNotification showing:NO];
}

- (void)adjustForKeyboard:(NSNotification*)aNotification showing:(BOOL)showing {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    NSDictionary* userInfo = [aNotification userInfo];
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect rect = self.view.frame;
    CGPoint activeOrgin = [[self.activeUIComponent superview] convertRect: self.activeUIComponent.frame toView: self.view].origin;
    
    if (showing){
        CGFloat viewOffset = activeOrgin.y + self.activeUIComponent.frame.size.height;
        CGFloat keyboardOffset = self.view.frame.size.height - keyboardSize.height;
        
        if (viewOffset < keyboardOffset){
            return;
        }
    }
    
    if (showing)
    {
        rect.origin.y -= keyboardSize.height;
        rect.size.height += keyboardSize.height;
    }
    else
    {
        if (rect.origin.y < 0){
            rect.origin.y += keyboardSize.height;
            rect.size.height -= keyboardSize.height;
        }
    }
    self.view.frame = rect;
    [UIView commitAnimations];
}

#pragma mark - UIViewControllerTransitioningDelegate Methods

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source {
    
    Animator *animator = [Animator new];
    animator.presenting = YES;
    return animator;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    Animator *animator = [Animator new];
    return animator;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
