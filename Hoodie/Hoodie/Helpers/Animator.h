//
//  Animator.h
//  UggApp
//
//  Created by Tomasz Justa on 02.11.2014.
//  Copyright (c) 2014 Mobile 5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Animator : NSObject<UIViewControllerAnimatedTransitioning>
@property (nonatomic, assign, getter = isPresenting) BOOL presenting;

@end
