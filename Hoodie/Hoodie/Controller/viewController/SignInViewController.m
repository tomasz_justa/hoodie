//
//  SignInViewController.m
//  Hoodie
//
//  Created by Tomasz Justa on 20.05.2015.
//  Copyright (c) 2015 Monotix. All rights reserved.
//

#import "SignInViewController.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface SignInViewController ()

@end

@implementation SignInViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fbProfileDidChange:) name:FBSDKProfileDidChangeNotification object:nil];
}

-(void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Facebook
-(IBAction)facebookButtonDidSelect:(id)sender
{
    [self startLoading];
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile",@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            [self showMessage:NSLocalizedString(@"FB_LOGIN_ERROR", nil)];
        } else if (result.isCancelled) {
            [self showMessage:NSLocalizedString(@"FB_LOGIN_CANCELLED", nil)];
        } else {
            if ([result.grantedPermissions containsObject:@"email"]) {
                [self fbLoginDidComplete];
                
            } else {
                [self showMessage:NSLocalizedString(@"FB_LOGIN_PERMISIONS", nil)];
            }
        }
    }];
}

-(void) fbProfileDidChange:(NSNotification*)notification
{
    //NSLog(@"FB login completed: \n token: \n%@ \n userID: %@",[FBSDKAccessToken currentAccessToken].tokenString,[FBSDKProfile currentProfile].userID);
    [self fbLoginDidComplete];
}

-(void) fbLoginDidComplete
{
    GTLQueryHoodieApi *query = [GTLQueryHoodieApi queryForFacebookLoginWithFacebookAccessToken:[FBSDKAccessToken currentAccessToken].tokenString];
    [self.service executeQuery:query completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
        [self stopLoading];
        if (error){
            [self showMessage:NSLocalizedString(@"FB_LOGIN_ERROR", nil)];
        }
        NSLog(@"data: %@",object);
    }];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"LogInSegue"])
    {
        LoginViewController *vc = segue.destinationViewController;
        vc.transitioningDelegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"RegisterSegue"])
    {
        RegisterViewController *vc = segue.destinationViewController;
        vc.transitioningDelegate = self;
    }
}



@end
