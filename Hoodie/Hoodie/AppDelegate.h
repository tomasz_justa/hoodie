//
//  AppDelegate.h
//  Hoodie
//
//  Created by Tomasz Justa on 20.05.2015.
//  Copyright (c) 2015 Monotix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

