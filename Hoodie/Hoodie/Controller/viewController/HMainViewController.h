//
//  HMainViewController.h
//  Hoodie
//
//  Created by Tomasz Justa on 20.05.2015.
//  Copyright (c) 2015 Monotix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Animator.h"
#import "GTLQueryHoodieApi.h"
#import "GTLServiceHoodieApi.h"
#import "Constans.h"


@interface HMainViewController : UIViewController<UIViewControllerTransitioningDelegate>
@property (nonatomic) GTLServiceHoodieApi *service;
@property (nonatomic, strong) UIView *activeUIComponent;

-(void)showMessage:(NSString*)message;
-(void) messageDidHideComplete;

-(void) startLoading;
-(void) startLoadingWithMessage:(NSString *)message;
-(void) stopLoading;

@end
