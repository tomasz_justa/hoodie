//
//  TabBarViewController.m
//  Hoodie
//
//  Created by Tomasz Justa on 02.06.2015.
//  Copyright (c) 2015 Monotix. All rights reserved.
//

#import "TabBarViewController.h"
#import "RDVTabBarController.h"
#import "RDVTabBarItem.h"
#import "UserNearbyViewController.h"
#import "ShoutsViewController.h"
#import "FriendsViewController.h"
#import "ActivityViewController.h"
#import "ProfileViewController.h"

@interface TabBarViewController ()
{
    UserNearbyViewController *first;
}
@end

@implementation TabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self customizeInterface];
    [self setupViewControllers];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods

- (void)setupViewControllers {
    UIStoryboard *mainBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    first = [mainBoard instantiateViewControllerWithIdentifier:@"UserNearbyViewController"];
    UIViewController *firstNavigationController = [[UINavigationController alloc]
                                                   initWithRootViewController:first];
    
    ShoutsViewController *second = [mainBoard instantiateViewControllerWithIdentifier:@"ShoutsViewController"];
    UIViewController *secondNavigationController = [[UINavigationController alloc]
                                                   initWithRootViewController:second];
    FriendsViewController *third = [mainBoard instantiateViewControllerWithIdentifier:@"FriendsViewController"];
    UIViewController *thirdNavigationController = [[UINavigationController alloc]
                                                   initWithRootViewController:third];
    ActivityViewController *fourth = [mainBoard instantiateViewControllerWithIdentifier:@"ActivityViewController"];
    UIViewController *fourthNavigationController = [[UINavigationController alloc]
                                                   initWithRootViewController:fourth];
    
    RDVTabBarController *tabBarController = [[RDVTabBarController alloc] init];
    [tabBarController setViewControllers:@[firstNavigationController, secondNavigationController,
                                           thirdNavigationController, fourthNavigationController]];
    [self addChildViewController:tabBarController];
    [[self view] addSubview:[tabBarController view]];
    [tabBarController didMoveToParentViewController:self];
    
    [self customizeTabBarForController:tabBarController];
    
    [second viewDidLoad];
    [third viewDidLoad];
    [fourth viewDidLoad];
    
}

- (void)customizeTabBarForController:(RDVTabBarController *)tabBarController {

    UIImage *finishedImage = [UIImage imageNamed:@"tabbar_selected_background"];
    UIImage *unfinishedImage = [UIImage imageNamed:@"tabbar_normal_background"];
    NSArray *tabBarItemImages = @[@"ic_tab_nearby_users", @"ic_tab_shouts", @"ic_tab_friends", @"ic_tab_chats"];
    
    NSInteger index = 0;
    for (RDVTabBarItem *item in [[tabBarController tabBar] items]) {
        [item setBackgroundSelectedImage:finishedImage withUnselectedImage:unfinishedImage];
        UIImage *selectedimage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_checked.png",
                                                      [tabBarItemImages objectAtIndex:index]]];
        UIImage *unselectedimage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_normal.png",
                                                        [tabBarItemImages objectAtIndex:index]]];
        [item setFinishedSelectedImage:selectedimage withFinishedUnselectedImage:unselectedimage];
        
        index++;
    }
}

- (void)customizeInterface {
    UINavigationBar *navigationBarAppearance = [UINavigationBar appearance];
    
    UIImage *backgroundImage = nil;
    NSDictionary *textAttributes = nil;
    
    backgroundImage = [UIImage imageNamed:@"navigationbar_background_tall"];
    
    textAttributes = @{
                       NSFontAttributeName: [UIFont boldSystemFontOfSize:18],
                       NSForegroundColorAttributeName: [UIColor blackColor],
                       };
    
    [navigationBarAppearance setBackgroundImage:backgroundImage
                                  forBarMetrics:UIBarMetricsDefault];
    [navigationBarAppearance setTitleTextAttributes:textAttributes];
    [navigationBarAppearance setShadowImage:[[UIImage alloc] init]];
    
    UIImageView *imageView = [[UIImageView alloc]
                              initWithFrame:CGRectMake(0,0,88,31)];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = NO;
    imageView.image = [UIImage imageNamed:@"toolbar_logo.png"];
    self.navigationItem.titleView = imageView;
    
    UIButton *settingsButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [settingsButton setImage:[UIImage imageNamed:@"avatar.jpg"] forState:UIControlStateNormal];
    settingsButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
    settingsButton.imageView.clipsToBounds = YES;
    [settingsButton addTarget:self action:@selector(settingsDidSelect) forControlEvents:UIControlEventTouchUpInside];
    [settingsButton setFrame:CGRectMake(0,0,32,32)];
    settingsButton.imageView.layer.cornerRadius = settingsButton.frame.size.width/2;
    UIBarButtonItem *searchBarButton = [[UIBarButtonItem alloc] initWithCustomView:settingsButton];

    self.navigationItem.rightBarButtonItem = searchBarButton;
}

-(void) settingsDidSelect
{
    UIStoryboard *mainBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProfileViewController *vc = [mainBoard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
