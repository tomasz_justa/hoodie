//
//  ShoutTableViewCell.m
//  Hoodie
//
//  Created by Tomasz Justa on 01.06.2015.
//  Copyright (c) 2015 Monotix. All rights reserved.
//

#import "ShoutTableViewCell.h"

@implementation ShoutTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.width/2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
