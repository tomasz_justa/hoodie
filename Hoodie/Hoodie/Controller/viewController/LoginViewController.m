//
//  LoginViewController.m
//  Hoodie
//
//  Created by Tomasz Justa on 21.05.2015.
//  Copyright (c) 2015 Monotix. All rights reserved.
//

#import "LoginViewController.h"
#import "SignInViewController.h"
#import "TabBarViewController.h"

@interface LoginViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nickLabel;
@property (weak, nonatomic) IBOutlet UITextField *passwdLabel;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeUIComponent = textField;
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"LoginBack"])
    {
        SignInViewController *vc = segue.destinationViewController;
        vc.transitioningDelegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"StartSegue"])
    {
        TabBarViewController *vc = segue.destinationViewController;
        vc.transitioningDelegate = self;
    }
}

@end
