//
//  FriendSectionTableViewCell.h
//  Hoodie
//
//  Created by Tomasz Justa on 02.06.2015.
//  Copyright (c) 2015 Monotix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendSectionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLAbel;

@end
