//
//  ShoutExpandTableViewCell.h
//  Hoodie
//
//  Created by Tomasz Justa on 01.06.2015.
//  Copyright (c) 2015 Monotix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoutExpandTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;

@end
