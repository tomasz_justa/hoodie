//
//  FriendsViewController.m
//  Hoodie
//
//  Created by Tomasz Justa on 01.06.2015.
//  Copyright (c) 2015 Monotix. All rights reserved.
//

#import "FriendsViewController.h"
#import "FriendRequestTableViewCell.h"
#import "FriendTableViewCell.h"
#import "FriendSectionTableViewCell.h"
#import "RDVTabBarController.h"
#import "RDVTabBarItem.h"

@interface FriendsViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation FriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup View
-(void) setupView
{
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [[self rdv_tabBarItem] setBadgeValue:@"121"];
}

#pragma mark - TableView Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0){
        return 5;
    } else {
        return 20;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *identifer = @"FriendSectionCell";
    
    FriendSectionTableViewCell *cell = (FriendSectionTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifer];
    if(!cell){
        UINib *nib = [UINib nibWithNibName:identifer bundle:nil];
        [tableView registerNib:nib forCellReuseIdentifier:identifer];
        cell = [tableView dequeueReusableCellWithIdentifier:identifer];
    }
    
    if (section==0){
        cell.headerTitleLAbel.text = @"FriendRequest (5)";
        cell.contentView.backgroundColor = [UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1.0];
    } else {
        cell.headerTitleLAbel.text = @"Firends (20)";
        cell.contentView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
/*
-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section==0){
        return @"FriendRequest(5)";
    } else {
        return @"Firends(20)";
    }
}
*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier;
    if (indexPath.section == 0) {
        identifier = @"FriendRequestCell";
        FriendRequestTableViewCell *cell = (FriendRequestTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
        if(!cell){
            UINib *nib = [UINib nibWithNibName:identifier bundle:nil];
            [tableView registerNib:nib forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        return cell;
    } else {
        identifier = @"FriendCell";
        FriendTableViewCell *cell = (FriendTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
        if(!cell){
            UINib *nib = [UINib nibWithNibName:identifier bundle:nil];
            [tableView registerNib:nib forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        return cell;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
